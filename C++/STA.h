#ifndef STA_H_INCLUDED
#define STA_H_INCLUDED
//Statistics library
#include <algorithm>    // std::sort
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <functional>
#include <limits>
#include <cassert>
#include <iostream>


double MEDIAN(std::vector<double> val);

double MEAN(std::vector<double> val);

double ST_DEV(std::vector<double> val);
double F_ST_DEV(std::vector<double> val, double mean);

double I_MEDIAN(double mean, uint nvmean, double val);//Incremenal average
double D_MEDIAN(double mean, uint nvmean, double val);//Decremental average

#endif
