#ifndef DIF_H_INCLUDED
#define DIF_H_INCLUDED
#include <vector>
#include <functional>
#include <iostream>

//M�todos de diferenciacion, integracion y calculo diferencial numericos

const double eps=1e-10;

//f:R->R
double DRV(std::function<double(double)> fun, double point);

double DRV2(std::function<double(double)> fun, double point);

double INTEG(std::function<double(double)> fun, double iP, double fP, int n);//trapecio compuesta
//f:R->R


//f:R^n->R
std::vector<double> GRAD(std::function<double(std::vector<double>)> fun, std::vector<double>point);//Vector gradiente


std::vector<std::vector<double>> HESS(std::function<double(std::vector<double>)> fun, std::vector<double>point);//Matriz hessiana

    

//f:R^n->R


//f:R^n->R^n

//f:R^n->R^n
#endif
