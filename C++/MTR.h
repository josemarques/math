#ifndef MTR_H_INCLUDED
#define MTR_H_INCLUDED

#include <vector>
#include <iostream>

#include "VEC.h"

//C�lculo matricial

std::vector<std::vector<double>> SM(std::vector<std::vector<double>> A,std::vector<std::vector<double>> B);//Suma matricial


std::vector<std::vector<double>> RM(std::vector<std::vector<double>> A,std::vector<std::vector<double>> B);//Resta matricial


std::vector<std::vector<double>> PEM(std::vector<std::vector<double>> A,double e);//Producto escalar matricial


std::vector<std::vector<double>> PM(std::vector<std::vector<double>> A,std::vector<std::vector<double>> B);//Producto matricial
    

std::vector<std::vector<double>> SWP(std::vector<std::vector<double>>M, int a, int b);//SWAP rows of a matrix


std::vector<std::vector<double>> TRS(std::vector<std::vector<double>>M);//Devuelve la traspuesta de la matriz
std::vector<std::vector<float>> TRS(std::vector<std::vector<float>>M);//Devuelve la traspuesta de la matriz


std::vector<std::vector<double>> PIV(std::vector<std::vector<double>>M, int fId);//Algoritmo de pivoteo matricial

	
std::vector<std::vector<double>> GAUSS_ESC(std::vector<std::vector<double>> M);//M�todo de gaus para obtenr matriz escalonada


std::vector<std::vector<double>> GAUSS_ESC_R(std::vector<std::vector<double>>M);//M�todo de gaus para obtenr matriz escalonada reducida



//double DET(std::vector<std::vector<double>> M);//Devuelve el determinante de la matriz

double DET(const std::vector<std::vector<double>>& matrix) ;

// Funci�n para calcular la matriz adjunta
std::vector<std::vector<double>> ADJUG(const std::vector<std::vector<double>>& matrix);

// Funci�n para calcular la inversa de una matriz
std::vector<std::vector<double>> INV(const std::vector<std::vector<double>>& matrix);



//std::vector<double> GAUSS_SS(std::vector<std::vector<double>>M,std::vector<double>b);
// std::vector<std::vector<double>> INV(std::vector<std::vector<double>> M);//M�todo matriz inversa por resolucion de sistemas, poco eficiente


#endif
