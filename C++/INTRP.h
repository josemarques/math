#ifndef INTRP_H_INCLUDED
#define INTRP_H_INCLUDED
//Statistics library

#include <iostream>
#include <vector>
#include <cmath>
#include <stdexcept>

#include "SLV.h"

double ILB(double x,double x1,double x2,double y1,double y2);//Interpolación lineal bidimensional

std::vector<double> POLADJ(const std::vector<double>& x, const std::vector<double>& y, int g);//Polinomial sdjustment
std::vector<float> POLADJ(const std::vector<float>& x, const std::vector<float>& y, int g);//Polinomial sdjustment

double UDPC(std::vector<double> coef, double x );//returns the y value given an x and the coeficients of the polinomiia. Unidimensional polinomial compute
float UDPC(std::vector<float> coef, float x );//returns the y value given an x and the coeficients of the polinomiia. Unidimensional polinomial compute

int INTRP_TEST();//Interpolation test

#endif
