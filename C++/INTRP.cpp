#include "INTRP.h"
//Statistics library

double ILB(double x,double x1,double x2,double y1,double y2){//Interpolación lineal bidimensional
    //DBG
    //std::cout<<"x: "<<x<<" x_1: "<<x1<<" x_2: "<<x2<<" y_1: "<<y1<<" y_2: "<<y2<<std::endl;
    //DBG

    double y=0;
    
    if(x2-x1!=0){
    y=(((x-x1)/(x2-x1))*(y2-y1))+y1;
    return y;
}
    else
    return y1;//If the interpolation is betwen de same x1 and x2 coordinate, it returns the y1 value
        
}




// Función para ajustar un polinomio de grado n a los datos de x e y
std::vector<double> POLADJ(const std::vector<double>& x, const std::vector<double>& y, int g){
    if (x.size() != y.size() || x.empty()) {
        throw std::invalid_argument("Los vectores x e y deben tener el mismo tamaño y no estar vacíos.");
    }

    int n = x.size();
    int m = g + 1;

    // Crear la matriz del sistema normal y el vector de términos independientes
    std::vector<std::vector<double>> A(m, std::vector<double>(m, 0.0));
    std::vector<double> b(m, 0.0);

    // Llenar A y b
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < m; ++j) {
            for (int k = 0; k < n; ++k) {
                A[i][j] += std::pow(x[k], i + j);
            }
        }
        for (int k = 0; k < n; ++k) {
            b[i] += y[k] * std::pow(x[k], i);
        }
    }

    // Resolver el sistema de ecuaciones para encontrar los coeficientes
    return GAUSS_LSS(A, b);
}

std::vector<float> POLADJ(const std::vector<float>& x, const std::vector<float>& y, int g) {
    if (x.size() != y.size() || x.empty()) {
        throw std::invalid_argument("Los vectores x e y deben tener el mismo tamaño y no estar vacíos.");
    }

    int n = x.size();
    int m = g + 1;

    // Crear la matriz del sistema normal y el vector de términos independientes
    std::vector<std::vector<float>> A(m, std::vector<float>(m, 0.0));
    std::vector<float> b(m, 0.0);

    // Llenar A y b
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < m; ++j) {
            for (int k = 0; k < n; ++k) {
                A[i][j] += std::pow(x[k], i + j);
            }
        }
        for (int k = 0; k < n; ++k) {
            b[i] += y[k] * std::pow(x[k], i);
        }
    }

    // Resolver el sistema de ecuaciones para encontrar los coeficientes
    return GAUSS_LSS(A, b);
}

double UDPC(std::vector<double> coef, double x ){//returns the y value guiven an x and the coeficients of the polinomiia. Unidimensional polinomial compute
	double y;
	if(coef.size()){
		y=0;
        for(uint i=0;i<coef.size();i++){
            y+=pow(x,i)*coef[i];
        }
	}

	return y;

}

float UDPC(std::vector<float> coef, float x ){//returns the y value guiven an x and the coeficients of the polinomiia. Unidimensional polinomial compute
	float y;
	if(coef.size()){
		y=0;
        	for(uint i=0;i<coef.size();i++){
		y+=pow(x,i)*coef[i];
	}
	}

	return y;

}

// Función de prueba
int INTRP_TEST() {
    // Datos de ejemplo
    std::vector<double> x = {1, 2, 3, 4, 5};
    std::vector<double> y = {2.2, 2.8, 3.6, 4.5, 5.1};

    int grado = 4;

    try {
        // Ajustar polinomio
        std::vector<double> coeficientes = POLADJ(x, y, grado);

        // Imprimir los coeficientes
        std::cout << "Coeficientes del polinomio de grado " << grado << ":\n";
        for (size_t i = 0; i < coeficientes.size(); ++i) {
            std::cout << "a" << i << " = " << coeficientes[i] << "\n";
        }

        std::cout << "Valores interpolados " << ":\n";
        std::vector<double> yr;
		for (size_t i = 0; i < x.size(); ++i) {
			yr.push_back(UDPC(coeficientes, x[i] ));
            std::cout << "x=" << x[i] << " yr= " << yr[i] << "\n";
        }

    } catch (const std::exception& e) {
        std::cerr << "Error: " << e.what() << "\n";
    }

    return 0;
}
