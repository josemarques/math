#ifndef DFT_H_INCLUDED
#define DFT_H_INCLUDED

//Fast Fourier Transform and Inverse Fast Fourier Transform
constexpr auto PI = 3.14159265;

std::vector<std::vector<double>> DFT(std::vector<std::vector<double>> data,double uni=1){//FFT bidimensional function [i]{t,Y}
    std::vector<std::vector<double>> Fv;//{f,r,th}
    if(data.size()>0&&data[0].size()==2){//Condiciones de funcionamiento.
    
   double tmin=data[0][0];
   for(int t=1;t<data.size();t++){
       if(tmin>data[t][0]){
           tmin=data[t][0];
        }
    }
   
   double tmax=data[0][0];
   for(int t=1;t<data.size();t++){
       if(tmax<data[t][0]){
           tmax=data[t][0];
        }
    }
    
    double tl=tmax-tmin;
	//tl = uni;//Override
   
   for(int f=0;f<data.size();f++){
    double fr=(double)f/(tl*2);//Current frecuency
    
    //WRAP
    std::vector<std::vector<double>> wrp;
    for(int i=0;i<data.size();i++){
        wrp.push_back({data[i][1]*cos(-2*PI*fr*data[i][0]),data[i][1]*sin(-2*PI*fr*data[i][0])});
    }
    //WRAP
    
    //CG
    double XCG=0;
    double YCG=0;
    double rCG=0;
    double thCG=0;
    
    for(int i=0;i<wrp.size();i++){
        XCG+=wrp[i][0];
        YCG+=wrp[i][1];        
    }
    rCG=sqrt(pow(XCG,2)+pow(YCG,2));

	if (YCG > 0 && XCG > 0)
		thCG = asin((double)YCG / rCG);
	else if (YCG > 0 && XCG < 0)
		thCG = PI - asin((double)YCG / rCG);
	else if (YCG < 0 && XCG < 0)
		thCG = PI - asin((double)YCG / rCG);
	else
		thCG =- acos((double)XCG / rCG);
    //CG
    
    Fv.push_back({fr,rCG,thCG});
   }
    
    }
    return Fv;
    
}



std::vector<std::vector<double>>IDFT(std::vector<std::vector<double>> data, double fi, double ff, double ti, double tf) {//data{f,r,th}
	std::vector<std::vector<double>>res;
	if (data.size() > 0) {
		if (data[0].size() == 3) {

			for (double e = 0; e < (tf - ti); e += 1 / (ff * 20)) {
				res.push_back({ ti + e,0});//{x,y}
			}

			for (int i = 0; i < data.size(); i++) {
				if (data[i][0] >= fi && data[i][0] <= ff) {


					for (int k = 0; k < res.size(); k ++) {
						res[k][1] += data[i][1] * sin(-((data[i][0] * (2 * PI) * (res[k][0] + ti)) + data[i][2]));
					}



				}

			}
		}
	}
	return res;
}

#endif
