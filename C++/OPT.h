#ifndef OPT_H_INCLUDED
#define OPT_H_INCLUDED
#include <iomanip>
#include <vector>
#include <functional>
#include <iostream>

#include "DIF.h"
#include "NRM.h"
#include "VEC.h"
#include "MTR.h"

//Optimization algorithms
std::vector<double> S_GDO(std::function<double(std::vector<double>)> fun, std::vector<double>X0,int MxIt, double min);

std::vector<double> NEUR_GDO(std::function<double(std::vector<double>)> fun, std::vector<double>X0,int MxIt, double min);//Gradient descent optimicer A1 for sigmoid functions.

std::vector<double> NEUR_GDO(std::function<double(std::vector<double>)> fun, std::vector<double>X0,int MxIt, double min,double GrdLmt, double s=1.001, double B=0.05255,double* CNG=0, double* CFV=0 );//Gradient descent optimicer A1 for sigmoid functions.

std::vector<double> V_GDO(std::function<double(std::vector<double>)> fun, std::vector<double>X0,int MxIt, double min);//Gradient descent optimicer A1

std::vector<double> BFGS(std::function<double(std::vector<double>)> fun,std::vector<double> X0,int MxIt, double min);


#endif
