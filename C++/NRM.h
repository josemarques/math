#ifndef NRM_HPP_INCLUDED
#define NRM_HPP_INCLUDED
#include <vector>
#include <cassert>

//Data normalizer
class NRM{
public:
    double Nrt=0;//Maximun
    double Mnt=0;//Minimun
    double Rgt=0;//Rang Training data
    
    double nR=1.2;
    NRM(){
       /* Nrt=0;//Maximun
        Mnt=0;//Minimun
        Rgt=0;//Rang Training data*/
    }
    
    void ONP(std::vector<double> veec);//Obtain normalizer parametres
    
    std::vector<double> RNV(std::vector<double> veec);//Return the normalized vector

    double RN(double veec);//Return denormalized single data

};

std::vector <double> NRMLZ(std::vector <double> vector);//Normalization of a vector to values betweemn 0 and 1

#endif
