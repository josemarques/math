#include "NRM.h"

//Data normalizer
    void NRM::ONP(std::vector<double> veec){//Obtain normalizer parametres
        if(veec.size()>0){
        //Obtain the maximuns and minimun
            Nrt=veec[0];
            Mnt=veec[0];
            for(int dbli=0;dbli<veec.size();dbli++){
                if(Nrt<veec[dbli]){
                    Nrt=veec[dbli];
                    //std::cout<<"Nrt: "<<Nrt<<std::endl;
                }
                if(Mnt>veec[dbli]){
                    Mnt=veec[dbli];
                    //std::cout<<"Mnt: "<<Mnt<<std::endl;
                }
            }
            Rgt=Nrt-Mnt;
        }
    }
    
    std::vector<double> NRM::RNV(std::vector<double> veec){//Return the normalized vector
        
        std::vector<double> normalized;
        if(Rgt>0){
            for(int c=0;c<veec.size();c++){
                normalized.push_back((double)(veec[c]-Mnt)/(Rgt*nR));
            } 
        }
        else{
            for(int c=0;c<veec.size();c++){
                normalized.push_back(0);
            }
        }
        return normalized;
    }
    
    double NRM::RN(double veec){//Return denormalized single data
        
        double denormalized;
        if(Rgt>0){
                denormalized=(double)((veec*(Rgt*nR))+Mnt);
        }
        else{
                denormalized=0;
        
        }
        return denormalized;
    }



std::vector<double> NRMLZ(std::vector<double> vector){//Return the normalized vector
	assert(vector.size()>0);

	std::vector<double> normalized;

	double max=vector[0];
	double min=vector[0];

	for (int i=1;i<vector.size();i++){
		if(max<vector[i]){
			max=vector[i];
		}
		if(min>vector[i]){
			min=vector[i];
		}

	}

	//std::cout<<"max="<<max<<" min="<<min<<std::endl;

	for (int i=0;i<vector.size();i++){
		normalized.push_back((vector[i]-min)/(max-min));
	}

return normalized;
}
