#ifndef FPU_H_INCLUDED
#define FPU_H_INCLUDED

#include <vector>
#include <stdlib.h>

//Floating point utilities
bool FPC(std::vector<double>Va, std::vector<double>Vb, double tol);
bool FPC(float A, float B, float epsilon);
bool IPC(int A, int B, float epsilon);
int RIP(double data, int multip ,int prec);
int RIP(float data, int multip ,int prec);
int RIP(int data,int prec);

#endif
