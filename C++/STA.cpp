#include "STA.h"

double MEDIAN(std::vector<double> val){
    std::vector<double> SORTV=val;
    std::sort (SORTV.begin(), SORTV.end());
    return SORTV[(int)(SORTV.size()/2)];
}


double MEAN(std::vector<double> val){
    assert(val.size());

    double medi=0;
    for(int i=0;i<val.size();i++){
        medi+=val[i];
    }
    medi=medi/val.size();

    return medi;

}

double ST_DEV(std::vector<double> val){
    assert(val.size());
    if(val.size()>1){
    double medi=MEAN(val);
    double st_dev=0;
    for(int i=0;i<val.size();i++){
        st_dev+=pow((val[i]-medi),2);
    }
    st_dev=st_dev/val.size();
    st_dev=sqrt(st_dev);
    
    return st_dev;
        
    }
    else if(val.size()==1){
        return 0;
    }

    }

double F_ST_DEV(std::vector<double> val, double mean){
    assert(val.size());
    if(val.size()>1){
    double st_dev=0;
    for(int i=0;i<val.size();i++){
        st_dev+=pow((val[i]-mean),2);
    }
    st_dev=st_dev/val.size();
    st_dev=sqrt(st_dev);

    return st_dev;

    }
    else if(val.size()==1){
        return 0;
    }

    }

double I_MEDIAN(double mean, uint nv, double val){//Incremenal average

    return((mean*nv+val)/(nv+1));

}
double D_MEDIAN(double mean, uint nv, double val){//Incremenal average
    if(nv<=1){
        return val;
    }

    else{
    return((mean*nv-val)/(nv-1));
    }

}

