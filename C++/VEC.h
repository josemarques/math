#ifndef VEC_H_INCLUDED
#define VEC_H_INCLUDED

#include <vector>
#include <iostream>
#include <math.h>

std::vector<double> SV(std::vector<double> A,std::vector<double> B);//Suma vectorial

std::vector<double> RV(std::vector<double> A,std::vector<double> B);//Resta vectorial

std::vector<double> PEV(std::vector<double> A,double e);//Producto escalar vectorial

double NORM(std::vector<double> A);//Norma 2 vectorial


#endif
