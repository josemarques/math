#include "SLV.h"

//Solver algorithms

// std::vector<double> GAUSS_SS(std::vector<std::vector<double>>M){//M�todo de gaus para resoluci�n de sistemas lineales, M matriz ampliada
//     M=GAUSS_ESC(M);
//             std::vector<double> x;
//     for (int b=0;b<M.size();b++){
//         x.push_back(M[b][M.size()-1]);
//     }
//
//         double rT=0;
//
//         if(M.size()+1<M[0].size()){
//             std::cout<<"Sistema indeterminado";
//             exit (EXIT_FAILURE);
//         }
//
//         for(int i=M.size()-1;i>=0;i--){
//             rT=M[i][M[0].size()-1];
//             for(int j=i+1;j<M.size();j++){
//                 rT-=(x[j]*M[i][j]);
//             }
//
//             x[i]=rT/M[i][i];
//
//         }
//         return x;
// }
//
// std::vector<double> GAUSS_SS(std::vector<std::vector<double>>M,std::vector<double>b){//M�todo de gaus para resoluci�n de sistemas lineales, M matriz de funciones, b matriz de coeficientes
//     std::vector<std::vector<double>>Ma=M;
//
//     for(int f=0;f<M[0].size();f++){
//         Ma[f].push_back(b[f]);
//     }
//
//     return GAUSS_SS(Ma);
// }


//CHATGPT
// Funci�n para resolver un sistema de ecuaciones lineales Ax = b mediante eliminaci�n de Gauss
std::vector<double> GAUSS_LSS(std::vector<std::vector<double>> A, std::vector<double> b) {
    int n = A.size();
    if (n == 0 || A[0].size() != n || b.size() != n) {
        throw std::invalid_argument("El sistema debe ser cuadrado y compatible.");
    }

    // Aumentar la matriz con el vector b (matriz aumentada)
    for (int i = 0; i < n; ++i) {
        A[i].push_back(b[i]);
    }

    // Eliminaci�n Gaussiana
    for (int i = 0; i < n; ++i) {
        // Buscar el m�ximo pivote en la columna actual
        int maxRow = i;
        for (int k = i + 1; k < n; ++k) {
            if (std::fabs(A[k][i]) > std::fabs(A[maxRow][i])) {
                maxRow = k;
            }
        }

        // Intercambiar filas
        std::swap(A[i], A[maxRow]);

        // Verificar que el pivote no sea cero
        if (std::fabs(A[i][i]) < 1e-10) {
            throw std::runtime_error("El sistema no tiene soluci�n �nica.");
        }

        // Normalizar la fila actual
        for (int k = i + 1; k <= n; ++k) {
            A[i][k] /= A[i][i];
        }

        // Reducir las filas restantes
        for (int k = i + 1; k < n; ++k) {
            double factor = A[k][i];
            for (int j = i; j <= n; ++j) {
                A[k][j] -= factor * A[i][j];
            }
        }
    }

    // Sustituci�n hacia atr�s
    std::vector<double> x(n);
    for (int i = n - 1; i >= 0; --i) {
        x[i] = A[i][n];
        for (int k = i + 1; k < n; ++k) {
            x[i] -= A[i][k] * x[k];
        }
    }
    return x;
}

// Funci�n para resolver un sistema de ecuaciones lineales Ax = b mediante eliminaci�n de Gauss
std::vector<float> GAUSS_LSS(std::vector<std::vector<float>> A, std::vector<float> b) {
    int n = A.size();
    if (n == 0 || A[0].size() != n || b.size() != n) {
        throw std::invalid_argument("El sistema debe ser cuadrado y compatible.");
    }

    // Aumentar la matriz con el vector b (matriz aumentada)
    for (int i = 0; i < n; ++i) {
        A[i].push_back(b[i]);
    }

    // Eliminaci�n Gaussiana
    for (int i = 0; i < n; ++i) {
        // Buscar el m�ximo pivote en la columna actual
        int maxRow = i;
        for (int k = i + 1; k < n; ++k) {
            if (std::fabs(A[k][i]) > std::fabs(A[maxRow][i])) {
                maxRow = k;
            }
        }

        // Intercambiar filas
        std::swap(A[i], A[maxRow]);

        // Verificar que el pivote no sea cero
        if (std::fabs(A[i][i]) < 1e-10) {
            throw std::runtime_error("El sistema no tiene soluci�n �nica.");
        }

        // Normalizar la fila actual
        for (int k = i + 1; k <= n; ++k) {
            A[i][k] /= A[i][i];
        }

        // Reducir las filas restantes
        for (int k = i + 1; k < n; ++k) {
            float factor = A[k][i];
            for (int j = i; j <= n; ++j) {
                A[k][j] -= factor * A[i][j];
            }
        }
    }

    // Sustituci�n hacia atr�s
    std::vector<float> x(n);
    for (int i = n - 1; i >= 0; --i) {
        x[i] = A[i][n];
        for (int k = i + 1; k < n; ++k) {
            x[i] -= A[i][k] * x[k];
        }
    }
    return x;
}
