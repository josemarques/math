#!/usr/bin/env python
#Control library
import math

import STAT
import LNAL

def PIDs(pv,sp,pst,kp,ki,kd):#PID controller, return control signal given pv=process value=[x,t], a setpoint sp=a o desired value, and the previous data of the sistem(ppv=previous process varible, ie=integrated error) of PIDs current data,kp, ki, kd are the pid control variables
    ppv=list(pst[0])#previous process value
    ie=float(pst[1])#acumulated integral error

    e=float(sp-pv[0])#Error
    IE=float(ie+(e*(pv[1]-ppv[1])))
    PPV=list(pv)
    pe=(sp-ppv[0])
    DE=(e-pe)/(pv[1]-ppv[1])
    CNTRLSIG=(kp*e+ki*IE+kd*DE)
    print("MATH.CNTR.PIDs "+"sp::"+str(sp)+" pv::"+str(pv)+" e::"+str(e)+" ie::"+str(IE)+" de::"+str(DE)+" CNTRLSIG::"+str(CNTRLSIG))

    return(CNTRLSIG,PPV,IE)
