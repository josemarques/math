#!/usr/bin/env python

import STAT

#Numerical methods

#Interpolation

#Regresion
def LINREG(xdata, ydata):
	assert len(xdata)==len(ydata) , "xdata and y data have to be the same length"
	#a0+x·a1=y
	XMED=STAT.MEAN(xdata)
	YMEAN=STAT.MEAN(ydata)
	
	n=len(xdata)
	
	S1=0
	for i in range(n):
		S1=S1+(xdata[i]*ydata[i])
	S1=S1-n*XMED*YMED

	S2=0
	for i in range(n):
		S2=S2+(xdata[i]**2)
	S2=S2-n*XMED**2
	
	a1=S1/S2
	a0=YMED-a1*XMED
	
	return a0,a1

def DRV(fa,xa,fb,xb):
	DERV=(fa-fb)/(xb-xa)
	return DERV

def INTG(fa,xa,fb,xb):
	XSPAN=abs(xb-xa)
	if(fa<fb):
		INTEG=fa*XSPAN+(fb-fa)*(XSPAN)
	elif(fb<fa):
		INTEG=fb*XSPAN+(fa-fb)*(XSPAN)
	else:
		INTEG=fa*XSPAN

	return INTEG
