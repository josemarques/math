#!/usr/bin/env python
import math

import STAT
import LNAL

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def FQCM(data, dmsp):#Frequency compressor. sp=desired minimun signal period
	#print(len(data))
	#print(data)
	assert len(data)==2 , "SIGPR.FQCM data dimension error: Expected data[2][n]"
	if(len(data[0])>1):
		#data[0] expected to be in incremental order
		XSPAN=max(data[0])-min(data[0])#Data span
		XSTEPS=math.floor(XSPAN/dmsp)#Number of data steps
		#assert XSTEPS>0 ,"SIGPR.FQCM desired period is bigger than the X data span"
	else:
		return[data[0],data[1]]

	CXD=[data[0][0]]#Compressed X Data

	CYD=[] #Compressed Y values
	CCYV=data[1][0:max(LNAL.FNDEL(data[0],data[0][0]+dmsp))+1]
	CYD.append(STAT.MEAN(CCYV))

	for i in range(1,XSTEPS):
		CXD.append(CXD[len(CXD)-1]+dmsp)
		PRVID=max(LNAL.FNDEL(data[0],CXD[len(CXD)-1]))
		ACVID=max(LNAL.FNDEL(data[0],CXD[len(CXD)-1]+dmsp))
		CCYV=data[1][PRVID+1:ACVID+1]
		print("CCYV1::"+str(CCYV))
		CYD.append(STAT.MEAN(CCYV))

	return [CXD,CYD]



def NSRMVa(data, rmrat):#Noise spikes remover algorithm a
	#
	M=STAT.MEAN(data)
	difs=[]
	for i in range(0, len(data)):
		difs.append(abs(M-data[i]))

		difs=list(sorted(difs))
		MXDIF=difs[int((len(difs)-1)*(1-rmrat))]

		cDATASET=[]

		for i in range(0, len(dataset)):
			if(abs(M-dataset[i])<=MXDIF):
				cDATASET.append(dataset[i])

	return cDATASET


def NSRMVb(data, rmrat):#Noise spikes remover algorithm b
	cDATA=list(data)
	for i in range(int(len(data)*(1-rmrat)), len(data)):#Number of iteration to obtain a remove ratio of rmrat
		MXDIF=0
		MXDIFn=0
		M=PY.STAT.MEAN(cDATA)
		for ii in range(0,len(cDATA)):
			if(abs(cDATA[ii]-M)>MXDIF):
				MXDIF=abs(cDATA[ii]-M)
				MXDIFn=ii

		del cDATA[MXDIFn]

	return cDATA

def NSRMVc(data, sdlimit):#Noise spikes remover algorithm c
	m=STAT.MEAN(data)
	cDATA=[]
	for i in range(0, len (data)):
		if(abs(data[i]-m)<sdlimit):
			cDATA.append(data[i])
	return cDATA

def NSRMVd(data, mxdesv=200000):#Noise spikes remover algorithm d
	cDATA=[]
	rmD=[]
	cDATA.append(data[0])
	for i in range(1,len (data)-1):
		if(abs(data[i-1]-data[i])>mxdesv and abs(data[i+1]-data[i])>mxdesv and abs(data[i+1]-data[i-1])<mxdesv):#Noise sppike detected. i=spike data point
			rmD.append(i)
			#print(bcolors.WARNING+"NOISE SPIKE DETECTED"+bcolors.ENDC)

		else:#Spike not detected
				cDATA.append(data[i])

	cDATA.append(data[len(data)-1])

	return cDATA,rmD

def NSRMVe(data,idtr,ntrmv):#removes ntrmv bigger absolutes of idtr in multidimensional list. Keeps the list order:: data format= [[data a0, data b0, data c0],[data a1, data b1, datac1],...,[data an, databn, data cn]]. idtr is refered to a, b or c...
	DATA=[]
	for i in range(len(data)):
		tDATA=[]
		for ii in range(len(data[i])):
			tDATA.append(float(data[i][ii]))
		DATA.append(tDATA)

	SIGNL=[]#Signal::List with [idtr] values.
	#Add original order id
	for i in range(len(DATA)):
		DATA[i].append(i)#Add order id as last item
		SIGNL.append(DATA[i][idtr])

	#Short list by desired idtr
	SORTBID = sorted(DATA, key=lambda SSET: SSET[idtr])

	SIGNL=PY.STAT.MEDIAN(SIGNL)#Median value of signal data

	#Check maximun deviated value ntrmv times and delete item.
	for i in range(ntrmv):
		DIFi=abs(SORTBID[0][idtr]-SIGNL)
		DIFf=abs(SORTBID[len(SORTBID)-1][idtr]-SIGNL)
		if(DIFi>DIFf):
			del SORTBID[0]
		else:
			del SORTBID[len(SORTBID)-1]

	#ReShort list to original oder
	SORTBID = sorted(SORTBID, key=lambda SSET: SSET[len(SSET)-1])

	#Remove original order
	CDATA=[]
	for i in range(len(SORTBID)):
		tCDATA=[]
		for ii in range(len(SORTBID[i])-1):
			tCDATA.append(SORTBID[i][ii])
		CDATA.append(tCDATA)

	return CDATA

def NSRMVf(data,idtr,ntrmv):#removes ntrmv bigger absolutes of idtr in multidimensional list. Keeps the list order:: data format= [[data a0, data b0, data c0],[data a1, data b1, datac1],...,[data an, databn, data cn]]. idtr is refered to a, b or c... nrm is the id of removed data
	DATA=[]
	RMID=[]
	for i in range(len(data)):
		tDATA=[]
		for ii in range(len(data[i])):
			tDATA.append(float(data[i][ii]))
		DATA.append(tDATA)

	SIGNL=[]#Signal::List with [idtr] values.
	#Add original order id
	for i in range(len(DATA)):
		DATA[i].append(i)#Add order id as last item
		SIGNL.append(DATA[i][idtr])

	#Short list by desired idtr
	SORTBID = sorted(DATA, key=lambda SSET: SSET[idtr])

	SIGNL=PY.STAT.MEDIAN(SIGNL)#Median value of signal data

	#Check maximun deviated value ntrmv times and delete item.
	for i in range(ntrmv):
		DIFi=abs(SORTBID[0][idtr]-SIGNL)
		DIFf=abs(SORTBID[len(SORTBID)-1][idtr]-SIGNL)
		if(DIFi>DIFf):
			RMID.append(SORTBID[0][len(SORTBID[0])-1])
			del SORTBID[0]
		else:
			RMID.append(SORTBID[len(SORTBID)-1][len(SORTBID[len(SORTBID)-1])-1])
			del SORTBID[len(SORTBID)-1]

	#ReShort list to original oder
	SORTBID = sorted(SORTBID, key=lambda SSET: SSET[len(SSET)-1])

	#Remove original order
	CDATA=[]
	for i in range(len(SORTBID)):
		tCDATA=[]
		for ii in range(len(SORTBID[i])-1):
			tCDATA.append(SORTBID[i][ii])
		CDATA.append(tCDATA)

	return [CDATA,RMVD]

def MDDD(data):#Most deviated data detector. Returns the ids of the most deviated value
	SIGNL=[]
	for i in range(len(data)):
		SIGNL.append([float(data[i]),i])

		#Short list by desired idtr
	SORTBID = sorted(DATA, key=lambda SSET: SSET[idtr])
	MEDIAN=PY.STAT.MEDIAN(SIGNL)#Median value of signal data

	#Check maximun deviated value and returns its id
	DIFi=abs(SORTBID[0][1]-MEDIAN)
	DIFf=abs(SORTBID[len(SORTBID)-1][1]-MEDIAN)
	if(DIFi>DIFf):
		return SORTBID[0][1]
	else:
		return SORTBID[len(SORTBID)-1][1]
