 # -*- coding: utf-8 -*-
#!/usr/bin/env python
#!/usr/bin/python -u

#COMB.py ->Combinatory methods library

def COMB2L(la,lb):#Returns possible combinations of 2 lists.
	COMB=[]
	for i in range(len(la)):
		for ii in range(len(lb)):
			COMB.append([la[i],lb[ii]])
	#print(COMB)
	return COMB

def COMBL(lists):#Combine all posible permutation between elements of subsets in the list [subset 0],[subset 1],[subset 2]...
	def CLLS(l):
		LIST=[]
		for i in range(len(l)):
			tLIST=list(l[i][0])
			tLIST.append(l[i][1])
			LIST.append(tLIST)
		return LIST

	COMB=COMB2L(lists[0],lists[1])
	for i in range(2,len(lists)):
		COMB=COMB2L(COMB,lists[i])
		#print("PRECLLS")
		#print(COMB)
		COMB=CLLS(COMB)
		#print("POSTCLLS")
		#print(COMB)
	return COMB






def test(lists):
	print("TEST")
	print(COMBL(lists))



if __name__ == "__main__":
	test([[0,1],[2,3],["A","B"]])
