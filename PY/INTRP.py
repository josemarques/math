 # -*- coding: utf-8 -*-
#!/usr/bin/env python
#!/usr/bin/python -u

#INTERP.py ->Interpolations library

def LIN_INTERP(x0,x1,y0,y1,x):
	B=(y1-y0)/(x1-x0)
	A=y0-B*x0
	y=A+B*x
	return y

def LIN_INTERP_Fx(x0,x1,y0,y1,y=0):
	#print("x0="+str(x0)+" x1="+str(x1))
	#print("y0="+str(y0)+" y1="+str(y1))
	B=(y1-y0)/(x1-x0)
	A=y0-B*x0
	#print("A"+str(A))
	#print("B"+str(B))
	x=(y-A)/B
	return x
