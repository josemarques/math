# -*- coding: utf-8 -*-
#!/usr/bin/env python

import LNAL

#STAT.py -> Métodos matemáticos estadísticos

def MEDIAN(data):#Calculates the median of a list
	SDATA=list(data)
	SDATA.sort()
	return SDATA[int(len(SDATA)/2)]

#Mobile statistics
def MMEAN(lmean,cvalue,n):
	MEAN=((lmean*(n-1)+cvalue)/n)
	return MEAN

def MSDES(lsdesv, lmean, cvalue, n):
	ldif=abs(cvalue-lmean);
	SDESV=((lsdesv*(n-1)+ldif)/n)
	return SDESV
#Mobile statistics

#Lists methods
def MTXMEAN(mtx):#Calculates the mean of the same position values in multiple matrixes
	#n matrix,i coord, j coord
	assert isinstance(mtx, list), "mtx must be a list to compute MTXMEAN"
	mtxM=LNAL.CMTX(mtx[0])#Initialize mean matrix result

	for i in range(0,len(mtx[0])):#Row itterator
		for j in range(0,len(mtx[0][i])):#Colum iterator
			cVALUE=[]
			for n in range(0,len(mtx)):#Load all values of ij position value
				cVALUE.append(mtx[n][i][j])
			mtxM[i][j]=MEAN(cVALUE)
	return mtxM

def MTXSDES(mtx):#Calculates the standard deviation of the same position values in multiple matrixes
	#n matrix,i coord, j coord
	assert isinstance(mtx, list), "mtx must be a list to compute MTXSDES"
	mtxSD=LNAL.CMTX(mtx[0])#Initialize sd matrix result

	for i in range(0,len(mtx[0])):#Row itterator
		for j in range(0,len(mtx[0][i])):#Colum iterator
			cVALUE=[]
			for n in range(0,len(mtx)):#Load all values of ij position value
				cVALUE.append(mtx[n][i][j])
			mtxSD[i][j]=DESVST(cVALUE)
	return mtxSD

def rMTX(mtxA, mtxB):#Difference between matrixes
	RESULT=[]
	for i in range(len(mtxA)):#Row iterator
		RESULT.append(rLIST(mtxA[i],mtxB[i]))
	return RESULT

def rLIST(lA,lB):
    result=[]
    for i in range(0,len(lA)):
        result.append(lA[i]-lB[i])
    return result

def sLIST(lA,lB):
    result=[]
    for i in range(0,len(lA)):
        result.append(lA[i]+lB[i])
    return result

def TRANSPOSE2d(LMxN):#Transpose bidimentional lists
    LNxM=[]
    for i in range(0,len(LMxN)):
        tL=[]
        for ii in range(0, len(LMxN[i])):
            tL.append(LMxN[i][ii])
            
        LNxM.append(tL)
    
    return list(map(list, zip(*LNxM)))

def MEAN(DATA):
	if (len(DATA)>1):
		mean=0
		for i in range(0, len(DATA)):
			mean=mean+DATA[i]
		mean=mean/len(DATA)
		return mean
	else:
		return DATA[0]
    
def MEAND(DATA):
    data=TRANSPOSE2d(DATA)
    mean=[]
    for i in range(0,len(data)):
        mean.append(MEAN(data[i]))
    return mean
        
def DESVST(DATA):
    mean=MEAN(DATA)
    desvs=0
    for i in range(0, len(DATA)):
        desvs=desvs+abs(DATA[i]-mean)

    desvs=desvs/len(DATA)
    return desvs

def DESVSTD(DATA):
    data=TRANSPOSE2d(DATA)
    desvs=[]
    for i in range(0,len(data)):
        desvs.append(DESVST(data[i]))
    return desvs

def FND_V_L(vector,value):#Find value or previous on array
    npos=[]
    for i in range(1,len(vector)):
        if((vector[i-1]<=value and vector[i]>=value)):
            if(abs(vector[i-1]-value)<abs(vector[i]-value)):
                npos.append(i-1)
            else:
                npos.append(i)
    return npos

def FND_CLS_MN(DATA):#Returns the cosest value to the mean of all values on the vector
    mean=MEAND(DATA)
    minDIF=DATA[0]-mean
    VALUE=DATA[0]
    for i in range(0,len(DATA)):
        if(DATA[i]-mean<minDIF):
            minDIF=DATA[i]-mean
            VALUE=DATA[i]
    return VALUE

def RSQU(lr, li):#List real values, list interpolated values
	SSR=0
	for i in range(len(lr)):
		SSR=SSR+(lr[i]-li[i])**2

	SST=0
	yMEAN=MEAN(lr)
	for i in range(len(lr)):
		SST=SST+(lr[i]-yMEAN)**2

	RSQUARED=1-(SSR/SST)

	return RSQUARED

#Lists method

#Math methods

def LIN_INTERP(x0,x1,y0,y1,x):
    B=(y1-y0)/(x1-x0)
    A=y0-B*x0
    y=A+B*x
    return y

def LIN_INTERP_Fx(x0,x1,y0,y1,y=0):
    #print("x0="+str(x0)+" x1="+str(x1))
    #print("y0="+str(y0)+" y1="+str(y1))
    B=(y1-y0)/(x1-x0)
    A=y0-B*x0
    #print("A"+str(A))
    #print("B"+str(B))
    x=(y-A)/B
    return x

def rLIST(lA,lB):
    result=[]
    for i in range(0,len(lA)):
        result.append(lA[i]-lB[i])
    return result

def sLIST(lA,lB):
    result=[]
    for i in range(0,len(lA)):
        result.append(lA[i]+lB[i])
    return result

def aLIST(lA):#Return list with all absolute values
    result=[]
    for i in range(0,len(lA)):
        result.append(abs(lA[i]))
    return result

def SdLIST(lA,B):#Scalar divition, listA to scalar B
    result=[]
    for i in range(0,len(lA)):
        result.append(lA[i]/B)
    return result
#Math methods
