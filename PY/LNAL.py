#!/usr/bin/env python

def CMTX(mtx):#Copy of a matrix
	MTX=[]
	for i in range(len(mtx)):
		tMTX=[]
		for ii in range(len(mtx[i])):
			tMTX.append(mtx[i][ii])
		MTX.append(tMTX)
	return MTX

def CNM(r,c):#Create a empty matrix of r rows and c coulmns
	OM=[]
	for i in range(r):
		tOM=[]
		for ii in range(c):
			tOM.append(0)
		OM.append(tOM)
	return OM

#Lineal algebra methods
def IM(m):#Identity matrix of m x m
	RESULT=[]
	for i in range(m):
		tRESULT=[]
		for ii in range(m):
			if(i==ii):
				tRESULT.append(1)
			else:
				tRESULT.append(0)
		RESULT.append(tRESULT)
	return RESULT

def SxV(scalar, vector):#Scalar x vector
	RESULT=[]
	for i in range(len(vector)):
		RESULT.append(vector[i]*scalar)
	return RESULT

def SaV(scalar, vector):#Scalar + all elements on the vector
	RESULT=[]
	for i in range(len(vector)):
		RESULT.append(vector[i]+scalar)
	return RESULT

def VaV(vectora, vectorb):#Vector + vector
	RESULT=[]
	for i in range(len(vectora)):
		RESULT.append(vectora[i]+vectorb[i])
	return RESULT

def VeV(vectora, vectorb):#Vector - vector
	RESULT=[]
	for i in range(len(vectora)):
		RESULT.append(vectora[i]-vectorb[i])
	return RESULT


def MxV(matrix, vector):#Matrix X vector
	RESULT=[]
	for i in range(len(matrix)):#row iterator
		tRESULT=0
		for ii in range(len(matrix[i])):#column iterator
			tRESULT=tRESULT+(matrix[i][ii]*vector[ii])
		RESULT.append(tRESULT)
	return RESULT

def SxM(scalar, matrix):#Substract a scalar from all values of a matrix
	RESULT=[]
	for i in range(len(matrix)):#Row iterator
		tVECT=[]
		for ii in range(len(matrix[i])):#Column iterator
			tVECT.append((matrix[i][ii]*scalar))
		RESULT.append(tVECT)
	return RESULT

def TM(matrix):#Transpose matrix
    TRANSP=[]
    for i in range(0,len(matrix)):
        tL=[]
        for ii in range(0, len(matrix[i])):
            tL.append(matrix[i][ii])

        TRANSP.append(tL)

    return list(map(list, zip(*TRANSP)))

def MedMe(matrixa,matrixb):#Division elemnt by element between two matrices
	RESULT=[]
	for i in range(len(matrixa)):#Row iterator
		tVECT=[]
		for ii in range(len(matrixa[i])):#Column iterator
			tVECT.append((matrixa[i][ii]/matrixb[i][ii]))
		RESULT.append(tVECT)
	return RESULT

def SeM(scalar, matrix):#Substract a scalar from all values of a matrix
	RESULT=[]
	for i in range(len(matrix)):#Row iterator
		tVECT=[]
		for ii in range(len(matrix[i])):#Column iterator
			tVECT.append((matrix[i][ii]-scalar))
		RESULT.append(tVECT)
	return RESULT

def MsM(matrixa, matrixb):#Sum of two matrices
	RESULT=[]
	for i in range(len(matrixa)):#Row iterator
		tVECT=[]
		for ii in range(len(matrixa[i])):#Column iterator
			tVECT.append((matrixa[i][ii]+matrixb[i][ii]))
		RESULT.append(tVECT)
	return RESULT

def MeM(matrixa, matrixb):#Substraction of two matrices
	RESULT=[]
	for i in range(len(matrixa)):#Row iterator
		tVECT=[]
		for ii in range(len(matrixa[i])):#Column iterator
			tVECT.append((matrixa[i][ii]-matrixb[i][ii]))
		RESULT.append(tVECT)
	return RESULT

#Complex matrices methods
def Mreal(matrix):#Returns the real matrix part of a complex matrix
	RESULT=[]
	for i in range(len(matrix)):#Row iterator
		tVECT=[]
		for ii in range(len(matrix[i])):#Column iterator
			tVECT.append(matrix[i][ii].real)
		RESULT.append(tVECT)
	return RESULT

def Mimag(matrix):#Returns the imaginary matrix part of a complex matrix
	RESULT=[]
	for i in range(len(matrix)):#Row iterator
		tVECT=[]
		for ii in range(len(matrix[i])):#Column iterator
			tVECT.append(matrix[i][ii].imag)
		RESULT.append(tVECT)
	return RESULT

#Complex vector methods
def Vreal(vector):#Returns the real vector part of a complex matrix
	RESULT=[]
	for i in range(len(vector)):#Row iterator
		RESULT.append(vector[i].real)
	return RESULT

def Vimag(vector):#Returns the imaginary vector part of a complex matrix
	RESULT=[]
	for i in range(len(vector)):#Row iterator
		RESULT.append(vector[i].imag)
	return RESULT

#List methods
def FNDEL(vector, value):#Find equal or less value on list
	ID=[]
	for i in range(len(vector)):
		if(vector[i]<=value):
			ID.append(i)
	return ID

