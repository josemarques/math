#ifndef MATH_H_INCLUDED
#define MATH_H_INCLUDED

//Numerical algorithms, double floating point precission

#include <vector>
#include <stdio.h> 
#include <stdlib.h> 
#include <math.h>  
#include <time.h>
#include <string>
#include <algorithm>
#include <functional>
#include <limits>

#include "C++/FPU.h"//Floating point utilities
#include "C++/VEC.h"//Vector ops
#include "C++/MTR.h"//Matricial ops
#include "C++/DIF.h"//Diferential calculus
#include "C++/SLV.h"//Solvers algortithms
#include "C++/OPT.h"//Optimization algortithms
#include "C++/CMB.h"//Combinatory algorithms
#include "C++/STA.h"//Statistics
#include "C++/INTRP.h"//Interpolation algorithms
#include "C++/NRM.hpp"//Normalization algorithms
#include "C++/RND.h"//Random algorithms
#include "C++/DFT.h"//Fourier transforms algorithms


//COMMON ALGORITHMS
int factorial(int n){
  return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
}
//COMMON ALGORITHMS

#endif // AXFPL_H_INCLUDED
